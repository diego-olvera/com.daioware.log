package com.daioware.stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import com.daioware.file.MyRandomAccessFile;

public class PrinterFileTest {

	public static void assertCustomized(Object x,Object y) {
		assertCustomized(x,y,false);
	}
	public static void assertCustomized(Object x,Object y,boolean stopAppWhenFails) {
		try {
			assertEquals(x, y);
		}catch(Exception e) {
			if(stopAppWhenFails) {
				throw e;
			}
			else {
				e.printStackTrace();
			}
		}
	}
	public void test() throws Exception {
		final int MAX_PAGES=3;
		final int TIMES_TO_WRITE=6;
		final String CHARSET="utf-8";
		final String LAST_STRING="árbo";
		final String OTHER_STRING="Árbol";
		final int MAX_BYTES=OTHER_STRING.getBytes(CHARSET).length;
		final int LAST_STRING_LENGTH=LAST_STRING.getBytes(CHARSET).length;
		MyRandomAccessFile stream;
		PrinterFile printer=null;
		File auxFile;
		try{
			printer = new PrinterFile("log.log");
			printer.setMaxBytes(MAX_BYTES);
			printer.setMaxPages(MAX_PAGES);
			printer.setConsoleOut(false);
			for(int i=0;i<TIMES_TO_WRITE;i++) {
				printer.print(OTHER_STRING);
			}
			printer.print(LAST_STRING);
			assertCustomized(printer.getPrevFiles().size(),MAX_PAGES);
			for(File file:printer.getPrevFiles()) {
				stream=new MyRandomAccessFile(file,"rw");
				try {
					assertEquals(stream.length(),MAX_BYTES);
				}catch(AssertionError ex) {
					System.out.println(file.getAbsolutePath()+":"+stream.readAllAsUTF8());;
					ex.printStackTrace();
				}
				finally {
					stream.close();
					try {
						assertTrue(file.delete());
					}catch(AssertionError ex) {
						System.out.println(file.getAbsolutePath());
						ex.printStackTrace();
					}
				}
			}
					
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			stream=new MyRandomAccessFile(auxFile=printer.getFile(),"r");
			try {
				assertTrue(stream.length()==LAST_STRING_LENGTH);
			}
			finally {
				stream.close();
				printer.close();
				try {
					assertTrue(auxFile.delete());
				}catch(AssertionError ex) {
					System.out.println(auxFile.getAbsolutePath());
					ex.printStackTrace();
				}
			}	
		}
	}
	public static void main(String[] args) {
		PrinterFileTest test=new PrinterFileTest();
		try {
			test.test();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
