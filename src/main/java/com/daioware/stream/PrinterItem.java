package com.daioware.stream;

public class PrinterItem implements Printer{
	private Printer printer;

	public PrinterItem() {
		this(System.out::println);
	}
	public PrinterItem(Printer printer) {
		this.printer = printer;
	}

	public Printer getPrinter() {
		return printer;
	}

	public void setPrinter(Printer printer) {
		if(printer==null) {
			this.printer=Printer.emptyPrinter;
		}
		else {
			this.printer = printer;
		}
	}
	public void println(Object o) {
		printer.println(o);
	}
	public void print(Object o) {
		printer.println(o);
	}
}
