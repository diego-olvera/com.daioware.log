package com.daioware.stream;

public class OutputException extends Exception {
	private static final long serialVersionUID = 1L;

	public OutputException(String message) {
		super(message);
	}
	
	
}
