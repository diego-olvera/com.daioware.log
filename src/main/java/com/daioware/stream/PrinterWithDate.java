package com.daioware.stream;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.daioware.commons.Util;

public class PrinterWithDate implements Printer{
	public static final SimpleDateFormat defaultFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	private SimpleDateFormat format;

	public PrinterWithDate() {
		this(defaultFormat);
	}
	public PrinterWithDate(SimpleDateFormat df)  {
		setFormat(df);
	}
	
	public SimpleDateFormat getFormat() {
		return format;
	}

	public void setFormat(SimpleDateFormat format) {
		this.format = format;
	}	
	public void print(Object obj) {
		System.out.print(format.format(new Date())+":"+obj+Util.JUMP_LINE);
	}
	public void println(Object obj) {
		print(obj);
	}
	public static void main(String[] args) {
		Printer printer=null;
		try{
			printer = new PrinterWithDate();
			printer.println("Árbol");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if(printer!=null)printer.close();
		}
		
	}
}
