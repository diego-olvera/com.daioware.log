package com.daioware.stream;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import com.daioware.file.FileUtil;
import com.daioware.file.MyRandomAccessFile;

public class PrinterFile implements Printer{
	public static final long MAX_BYTES=5000000L;
	public static final int MAX_PAGES=5;
	
	public static final DateTimeFormatter dateFormat
		=DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss-SSS");
	private MyRandomAccessFile fileStream;
	private boolean consoleOut;
	private boolean addNumberToPageFile;
	private long maxBytes;
	private int maxPages;
	protected List<File> prevFiles;
	
	public PrinterFile(File file) throws IOException {
		this(new MyRandomAccessFile(file, "rw"));
	}
	public PrinterFile(String file) throws IOException {
		this(new File(file));
	}
	public PrinterFile(MyRandomAccessFile file) throws IOException {
		this(file,MAX_BYTES,MAX_PAGES);
	}
	public PrinterFile(MyRandomAccessFile file,long maxBytes,int maxPages) throws IOException {
		fileStream=file;
		fileStream.seek(file.length());
		setMaxBytes(maxBytes);
		setMaxPages(maxPages);
		prevFiles=new LinkedList<>();
		setAddNumberToPageFile(false);
	}
	
	public File getFile() {
		return fileStream.getFile();
	}
	public List<File> getPrevFiles() {
		return prevFiles;
	}
	public int getMaxPages() {
		return maxPages;
	}
	public void setMaxPages(int maxPages) {
		this.maxPages = maxPages;
	}
	public boolean isConsoleOut() {
		return consoleOut;
	}
	public void setConsoleOut(boolean consoleOut) {
		this.consoleOut = consoleOut;
	}
	
	public long getMaxBytes() {
		return maxBytes;
	}
	public void setMaxBytes(long maxBytes) {
		this.maxBytes = maxBytes;
	}
	
	public boolean isAddNumberToPageFile() {
		return addNumberToPageFile;
	}
	public void setAddNumberToPageFile(boolean addNumberToPageFile) {
		this.addNumberToPageFile = addNumberToPageFile;
	}
	@Override
	public synchronized void print(Object o) {
		try {
			String string=o.toString();
			fileStream.writeCharactersAsUTF8(string);
			if(isConsoleOut()) {
				System.out.print(string);
			}
			if(fileStream.length()>=getMaxBytes()) {
				paginateFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void paginateFile() throws IOException {
		fileStream.close();
		File originalFile=fileStream.getFile();
		String absPath=originalFile.getAbsolutePath();
		File file=new File(absPath);
		int prevFilesSize=prevFiles.size();
		File pagFile=new File(FileUtil.removeLastExtension(absPath)
				+" "+dateFormat.format(LocalDateTime.now(ZoneId.systemDefault()))
				+(isAddNumberToPageFile()?"_"+prevFilesSize:"")+"."+FileUtil.getLastExtension(file.getName()));
		//copying
		FileUtil.copyFile(originalFile, pagFile);
		fileStream=new MyRandomAccessFile(originalFile, "rw");
		fileStream.setLength(0);
		//renaming
		/*fileStream.getFile().renameTo(pagFile);
		fileStream=new MyRandomAccessFile(originalFile, "rw");
		*/
		prevFiles.add(pagFile);
		if(prevFilesSize+1>getMaxPages()) {
			prevFiles.remove(0).delete();		
		}
	}

	public void close() {
		try {
			fileStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
