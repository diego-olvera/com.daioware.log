package com.daioware.stream;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.daioware.commons.Util;
import com.daioware.file.MyRandomAccessFile;

public class PrinterFileWithDate extends PrinterFile{
	public static final DateTimeFormatter defaultFormat=DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	
	private DateTimeFormatter format;
	
	public PrinterFileWithDate(File file) throws IOException {
		this(file,defaultFormat);
	}
	public PrinterFileWithDate(File file,DateTimeFormatter df) throws IOException {
		super(file);
		setFormat(df);
	}
	public PrinterFileWithDate(MyRandomAccessFile file) throws IOException {
		this(file,defaultFormat);
	}
	public PrinterFileWithDate(MyRandomAccessFile file,DateTimeFormatter df) throws IOException {
		super(file);
		setFormat(df);
	}
	public PrinterFileWithDate(String file) throws IOException {
		this(file,defaultFormat);
	}
	public PrinterFileWithDate(String file,DateTimeFormatter df) throws IOException {
		super(file);
		setFormat(df);
	}
	public DateTimeFormatter getFormat() {
		return format;
	}

	public static DateTimeFormatter getDefaultformat() {
		return defaultFormat;
	}
	
	public void setFormat(DateTimeFormatter format) {
		this.format = format;
	}	
	public void print(Object obj) {
		String t=getFormat().format(LocalDateTime.now().atZone(ZoneId.systemDefault()))+":"+obj+Util.JUMP_LINE;
		super.print(t);
	}
	public void println(Object obj) {
		print(obj);
	}
	public static void main(String[] args) {
		PrinterFile printer=null;
		try{
			printer = new PrinterFileWithDate("log.txt");
			printer.println("Árbol");
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(printer!=null)printer.close();
		}
		
	}
}
