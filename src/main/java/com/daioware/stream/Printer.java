package com.daioware.stream;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.daioware.commons.Util;

@FunctionalInterface
public interface Printer {
	public static final String ERROR_TAG="ERROR:";
	public static final String INFO_TAG="INFO:";
	public static final String WARNING_TAG="WARNING:";
	public static final String DEBUG_TAG="DEBUG:";
	
	public static final Printer emptyPrinter=new Printer() {		
		@Override
		public void print(Object o) {
			
		}
	};
	public static final Printer defaultPrinter=new Printer() {		
		@Override
		public void print(Object o) {
			System.out.print(o);
		}
	};
	public static final Printer defaultPrinterWithDate=new PrinterWithDate();
	
	default boolean isErrorTagActive() {
		return true;
	}
	default boolean isInfoTagActive() {
		return true;
	}
	default boolean isWarningTagActive() {
		return true;
	}
	default boolean isDebugTagActive() {
		return true;
	}
	default void setErrorTagActive(boolean b) {
		
	}
	default void setInfoTagActive(boolean b) {
		
	}
	default void setWarningTagActive(boolean b) {
		
	}
	default void setDebugTagActive(boolean b) {
		
	}
	
	void print(Object o);
	
	default void printStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		printError(sw);
	}
	default void printError(Object o) {
		if(isErrorTagActive())println(ERROR_TAG+o);
	}
	default void printInfo(Object o) {
		 if(isInfoTagActive())println(INFO_TAG+o);
	}
	default void printWarning(Object o) {
		if(isWarningTagActive())print(WARNING_TAG+o);
	}
	default void printDebug(Object o) {
		if(isDebugTagActive())print(DEBUG_TAG+o);
	}
	default void println(Object o) {
		this.print(o+Util.JUMP_LINE);
	}
	
	default void close() {
	}
}
