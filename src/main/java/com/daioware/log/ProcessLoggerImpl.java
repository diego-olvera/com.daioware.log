package com.daioware.log;

import java.time.Instant;
import java.util.LinkedList;

import com.daioware.dateUtilities.TimeInterval;


public class ProcessLoggerImpl implements ProcessLogger{
	private LinkedList<TimeInterval> timeIntervals=new LinkedList<>();

	@Override
	public void storeTimeIntervals(Instant startDate, Instant finishDate) {
		timeIntervals.add(new TimeInterval(startDate, finishDate));		
	}

	@Override
	public Iterable<TimeInterval> getTimeIntervalIterator() {
		return timeIntervals;
	}

	@Override
	public long getRunningTimes() {
		return timeIntervals.size();
	}

	@Override
	public void emptyLog() {
		timeIntervals=new LinkedList<>();

	}

}
