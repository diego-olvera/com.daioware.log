package com.daioware.log;

import java.time.Instant;

import com.daioware.dateUtilities.TimeInterval;

public interface ProcessLogger {
	
	void storeTimeIntervals(Instant startDate,Instant finishDate);
	Iterable<TimeInterval> getTimeIntervalIterator();
	long getRunningTimes();
	void emptyLog();
}
